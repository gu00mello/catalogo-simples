import styled from "styled-components";

import { Colors } from "../../global/colors";

export const Button = styled.button`
  width: 95%;
  background: ${Colors.success_primary_color};
  border-radius: 10px;
  box-shadow: 0 0 10px 0 rgba(47 147 44 / 60%);
  font-size: 3.7vw;
  color: ${Colors.text_secundary_color};
  font-weight: bold;
  border: none;
  box-sizing: border-box;
  padding: 10px;
  margin: 25px auto;

  & {
    @media screen and (min-width: 1024px) {
      font-size: 16px;
      padding: 15px 10px;
    }
  }
`;
