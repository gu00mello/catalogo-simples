import styled from "styled-components";

import { Colors } from "../../global/colors";

export const Section = styled.section`
  width: 100%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-start;
  align-items: flex-start;
  box-sizing: border-box;
  padding: 0 10px;
  margin: 10px auto;

  & {
    @media screen and (min-width: 1024px) {
      justify-content: center;
    }
  }
`;

export const Card = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  background: ${Colors.background_clean_color};
  box-shadow: 0 2px 5px 0 rgba(0 0 0 / 8%);
  border-radius: 20px;
  border: 1px solid
    ${(props) =>
      props.inCart
        ? Colors.success_primary_color
        : props.inPromo
        ? Colors.button_secundary_color
        : Colors.background_clean_color};
  box-sizing: border-box;
  padding: 10px;
  margin: 0 0 10px 0;
  overflow: hidden;

  & {
    @media screen and (min-width: 1024px) {
      width: 45%;
      margin: 5px;
    }
  }
`;

export const Image = styled.img`
  width: 25vw;
  margin-right: 10px;

  & {
    @media screen and (min-width: 1024px) {
      width: 100px;
    }
  }
`;

export const Informations = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  box-sizing: border-box;
  margin: auto 0;
`;

export const Inline = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  box-sizing: border-box;
`;

export const Rate = styled.span`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  font-size: 3vw;
  color: ${Colors.text_description_primary_color};
  font-weight: 400;
  margin: 2px 0;

  & {
    @media screen and (min-width: 1024px) {
      font-size: 12px;
    }
  }
`;

export const Star = styled.img`
  width: 4vw;
  margin-right: 5px;

  & {
    @media screen and (min-width: 1024px) {
      width: 14px;
    }
  }
`;

export const Name = styled.h2`
  font-size: 4vw;
  color: ${Colors.text_name_primary_color};
  font-weight: bold;
  margin: 2px 0;

  & {
    @media screen and (min-width: 1024px) {
      font-size: 18px;
    }
  }
`;

export const Ingredients = styled.span`
  font-size: 3vw;
  color: ${Colors.text_description_secundary_color};
  font-weight: 400;
  margin: 2px 0;

  & {
    @media screen and (min-width: 1024px) {
      font-size: 13px;
    }
  }
`;

export const Price = styled.h3`
  font-size: 4vw;
  color: ${Colors.text_primary_color};
  font-weight: bold;
  margin: 2px 0;

  & {
    @media screen and (min-width: 1024px) {
      font-size: 18px;
    }
  }
`;

export const SubPrice = styled.span`
  font-size: 3.4vw;
  color: ${Colors.text_primary_color};
  font-weight: bold;
  margin: 2px 0 2px 5px;
  opacity: 0.5;
  text-decoration: line-through;

  & {
    @media screen and (min-width: 1024px) {
      font-size: 14px;
    }
  }
`;

export const AddToCart = styled.img`
  position: absolute;
  right: 10px;
  bottom: 10px;
  width: 8vw;

  & {
    @media screen and (min-width: 1024px) {
      width: 35px;
    }
  }
`;

export const Bubble = styled.div`
  position: absolute;
  top: -9px;
  right: -9px;
  display: ${(props) =>
    props.inCart ? "block" : props.inPromo ? "block" : "none"};
  font-size: 2.5vw;
  color: ${(props) =>
    props.inCart
      ? Colors.text_promo_primary_color
      : props.inPromo
      ? Colors.text_secundary_color
      : Colors.text_primary_color};
  background: ${(props) =>
    props.inCart
      ? Colors.success_primary_color
      : props.inPromo
      ? Colors.button_secundary_color
      : Colors.button_secundary_color};
  border-radius: 100px;
  padding: 15px 15px 7px 7px;

  & {
    @media screen and (min-width: 1024px) {
      font-size: 12px;
    }
  }
`;
