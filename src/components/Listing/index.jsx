import {
  Section,
  Card,
  Image,
  Informations,
  Rate,
  Star,
  Name,
  Ingredients,
  Price,
  SubPrice,
  AddToCart,
  Bubble,
  Inline,
} from "./styles";

const Listing = ({ pizzas, setPizzas, cart, setCart }) => {
  const stringifyPrice = (p) => {
    let final_price;

    switch (String(p).length) {
      case 5:
        final_price = String(p).replace(".", ",");
        break;
      case 4:
      case 3:
        final_price = String(p).replace(".", ",") + "0";
        break;
      default:
        final_price = String(p) + ",00";
        break;
    }

    return final_price;
  };

  const addItemToCart = (item, index) => {
    let arr = pizzas;

    item.in_cart = true;
    arr[index].in_cart = true;
    setCart([...cart, item]);
    setPizzas(arr);

    // Save in local storage
    localStorage.setItem("cart", JSON.stringify([...cart, item]));
  };

  const removeItem = (item, index) => {
    let arr = pizzas;

    arr[index].in_cart = false;
    setCart(cart.filter((e) => e.name !== item.name));
    setPizzas(arr);

    // Save in local storage
    localStorage.setItem(
      "cart",
      JSON.stringify(cart.filter((e) => e.name !== item.name))
    );
  };

  const checkInCart = (item) => {
    return cart.find((e) => e.name === item.name);
  };

  const checkInPromo = (item) => {
    if (item.in_day === true) {
      item.price_without_promo = item.price;      
      item.discount = (20 / 100) * item.price;
      item.price = item.price - (20 / 100) * item.price;
      item.in_day = false;
    }

    return item.price;
  };

  return (
    <Section>
      {pizzas.map((item, index) => {
        return (
          <Card
            key={index}
            inPromo={item.price_without_promo > 0}
            inCart={checkInCart(item) ? true : item.in_cart}
          >
            <Bubble
              inPromo={item.price_without_promo > 0}
              inCart={checkInCart(item) ? true : item.in_cart}
            >
              {checkInCart(item) ? "QUERO ESSE" : "PIZZA DO DIA"}
            </Bubble>
            <Image src={`/assets/pizzas/${item.thumb}`} alt={item.name} />
            <Informations>
              <Rate>
                <Star src="/assets/star.svg" alt="Rate" />
                {item.rate}
              </Rate>
              <Name>{item.name}</Name>
              <Ingredients>{item.ingredients}</Ingredients>
              <Inline>
                <Price>R$ {stringifyPrice(checkInPromo(item))}</Price>
                {item.price_without_promo > 0 && (
                  <SubPrice>
                    R$ {stringifyPrice(item.price_without_promo)}
                  </SubPrice>
                )}
              </Inline>
            </Informations>
            <AddToCart
              src={checkInCart(item) ? "/assets/check.svg" : "/assets/plus.svg"}
              alt="Add to cart"
              onClickCapture={() => {
                checkInCart(item)
                  ? removeItem(item, index)
                  : addItemToCart(item, index);
              }}
            />
          </Card>
        );
      })}
    </Section>
  );
};

export default Listing;
