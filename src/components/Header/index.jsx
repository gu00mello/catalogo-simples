import { Section, Logo, CartSection, Cart, Count } from "./styles";
import { useNavigate } from "react-router-dom";

const Header = ({ count = 0 }) => {
  const navigate = useNavigate();

  return (
    <Section>
      <Logo src="/assets/header/logo.png" alt="Pizza" />
      <CartSection>
        <Count>{count}</Count>
        <Cart
          onClickCapture={() => navigate("/cart")}
          src="/assets/header/cart.svg"
          alt="Cart"
        />
      </CartSection>
    </Section>
  );
};

export default Header;
