import styled from "styled-components";

import { Colors } from "../../global/colors";

export const Section = styled.header`
  width: 100%;
  max-width: 1280px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  box-sizing: border-box;
  padding: 10px;
  margin: 0 auto;
`;

export const Logo = styled.img`
  width: 10%;
  max-width: 100px;
`;

export const CartSection = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
`;

export const Cart = styled.img`
  width: 10%;

  & {
    @media screen and (min-width: 1024px) {
      width: 7%;
    }
  }
`;

export const Count = styled.span`
  position: absolute;
  top: 0;
  right: 0;
  font-size: 3vw;
  background: ${Colors.button_primary_color};
  color: ${Colors.text_promo_primary_color};
  border-radius: 100px;
  padding: 0 4px;
  font-weight: bold;

  & {
    @media screen and (min-width: 1024px) {
      font-size: 12px;
    }
  }
`;
