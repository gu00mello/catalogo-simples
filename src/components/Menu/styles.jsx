import styled from "styled-components";

import { Colors } from "../../global/colors";

export const Navigation = styled.nav`
  width: 90%;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  border-radius: 10px;
  background: ${Colors.background_primary_color};
  margin: 10px auto 0 auto;

  & {
    @media screen and (min-width: 1024px) {
      width: 600px;
    }
  }
`;

export const Item = styled.div`
  width: 20%;
  text-align: center;
  color: ${(props) =>
    props.selected ? Colors.text_secundary_color : Colors.text_primary_color};
  font-size: 2.5vw;
  font-weight: bold;
  background: ${(props) =>
    props.selected
      ? Colors.success_primary_color
      : Colors.background_primary_color};
  padding: 3vw;
  border-radius: ${(props) =>
    props.elementIndex === 0
      ? "10px 0 0 10px"
      : props.elementIndex === 3
      ? "0 10px 10px 0"
      : "none"};
  margin: 0;

  & {
    @media screen and (min-width: 1024px) {
      font-size: 12px;
      padding: 15px;

      &:hover {
        cursor: pointer;
        background: ${(props) =>
          props.selected
            ? Colors.success_primary_color
            : Colors.background_secundary_color};
      }
    }
  }
`;
