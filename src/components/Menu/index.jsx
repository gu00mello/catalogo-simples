import { useState } from "react";

import { Navigation, Item } from "./styles";

const Menu = ({ setPage }) => {
  const [itemSelected, setItemSelected] = useState(0);

  const selectPage = (n) => {
    setPage(n);
    setItemSelected(n);
  };

  return (
    <Navigation>
      <Item
        elementIndex={0}
        selected={itemSelected === 0 && true}
        onClickCapture={() => selectPage(0)}
      >
        Recheio
      </Item>
      <Item
        elementIndex={1}
        selected={itemSelected === 1 && true}
        onClickCapture={() => selectPage(1)}
      >
        Massa
      </Item>
      <Item
        elementIndex={2}
        selected={itemSelected === 2 && true}
        onClickCapture={() => selectPage(2)}
      >
        Tamanho
      </Item>
      <Item
        elementIndex={3}
        selected={itemSelected === 3 && true}
        onClickCapture={() => selectPage(3)}
      >
        Ingredientes
      </Item>
    </Navigation>
  );
};

export default Menu;
