import {
  Section,
  Title,
  Item,
  Infos,
  Informations,
  Inline,
  Clean,
} from "./styles";
import { Button } from "../../components/Buttons";

const Cart = ({ cart, setCart }) => {
  const stringifyPrice = (p) => {
    let final_price;

    switch (String(p).length) {
      case 5:
        final_price = String(p).replace(".", ",");
        break;
      case 4:
      case 3:
        final_price = String(p).replace(".", ",") + "0";
        break;
      default:
        final_price = String(p) + ",00";
        break;
    }

    return final_price;
  };

  const calculateTotal = (arr) => {
    return arr.reduce((acc, cur) => acc + cur.price, 0);
  };

  const calculateTotalDiscount = (arr) => {
    return arr
      .filter((item) => item.discount)
      .reduce((acc, cur) => acc + cur.discount, 0);
  };

  return (
    <Section>
      <Title>Seu carrinho possui {cart.length} item(s)</Title>
      {cart.map((item, index) => {
        return (
          <Item key={index}>
            <Infos>{item.name}</Infos>
            <Infos>R$ {stringifyPrice(item.price)}</Infos>
          </Item>
        );
      })}
      <Informations>
        <Inline>
          <Infos>Desconto:</Infos>
          <Infos>
            R$ {stringifyPrice(calculateTotalDiscount(cart))}
          </Infos>
        </Inline>
        <Inline>
          <Infos>Total para pagar:</Infos>
          <Infos>R$ {stringifyPrice(calculateTotal(cart))}</Infos>
        </Inline>
      </Informations>
      <Button>FINALIZAR PEDIDO</Button>
      <Clean
        onClickCapture={() => {
          setCart([]);
          localStorage.setItem("cart", JSON.stringify([]));
        }}
      >
        Limpar carrinho
      </Clean>
    </Section>
  );
};

export default Cart;
