import styled from "styled-components";

import { Colors } from "../../global/colors";

export const Section = styled.section`
  width: 100%;
  max-width: 600px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  box-sizing: border-box;
  margin: 0 auto;
`;

export const Title = styled.h1`
  font-size: 4vw;
  font-weight: bold;
  color: ${Colors.text_primary_color};
  margin: 15px auto;

  & {
    @media screen and (min-width: 1024px) {
      font-size: 20px;
    }
  }
`;

export const Item = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  box-sizing: border-box;
  padding: 10px;
  border-bottom: 0.5px solid #d4d1cb; 
`;

export const Infos = styled.h4`
  font-size: 3vw;
  font-weight: bold;
  color: ${Colors.text_primary_color};
  margin: 0;

  & {
    @media screen and (min-width: 1024px) {
      font-size: 16px;
    }
  }
`;

export const Informations = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  box-sizing: border-box;
  margin-top: 20px;
`;

export const Inline = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  box-sizing: border-box;
  padding: 0 10px;
`;

export const Clean = styled.span`
  font-size: 3.2vw;
  color: ${Colors.text_name_primary_color};
  font-weight: bold;
  margin: 0 auto;

  & {
    @media screen and (min-width: 1024px) {
      font-size: 16px;
    }
  }
`;