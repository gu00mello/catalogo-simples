import { Main, Navigation, Page, Next, Arrow } from "./styles";

import Menu from "../../components/Menu";
import Listing from "../../components/Listing";
import { useState } from "react";

const Home = ({ page, setPage, cart, setCart }) => {
  const [pizzas, setPizzas] = useState([
    {
      rate: 4.8,
      name: "Bauru",
      thumb: "bauru.png",
      ingredients: "Mussarela, presunto, requeijão, oregano, tomate",
      price: 20,
      price_without_promo: 0,
      discount: 0,
      in_day: true,
      in_cart: false,
    },

    {
      rate: 4.8,
      name: "Calabresa",
      thumb: "calabresa.png",
      ingredients: "Mussarela, calabresa e cebola, oregano, azeitona preta",
      price: 20,
      price_without_promo: 0,
      discount: 0,
      in_day: false,
      in_cart: false,
    },

    {
      rate: 4.8,
      name: "Catuperoni",
      thumb: "catuperoni.png",
      ingredients: "Mussarela, calabresa e cebola, oregano, azeitona preta",
      price: 20,
      price_without_promo: 0,
      discount: 0,
      in_day: false,
      in_cart: false,
    },

    {
      rate: 4.8,
      name: "Quatro queijos",
      thumb: "queijo.png",
      ingredients: "Mussarela, queijo, oregano, azeitona preta, catupiry",
      price: 20,
      price_without_promo: 0,
      discount: 0,
      in_day: false,
      in_cart: false,
    },
  ]);

  const pages = {
    0: "Recheios",
    1: "Massas",
    2: "Tamanhos",
    3: "Ingredientes",
  };

  return (
    <Main>
      <Menu setPage={setPage} />
      <Navigation>
        <Page>{pages[page]}</Page>
        <Next>
          ESCOLHA A MASSA <Arrow src="/assets/arrow.svg" alt="Next" />
        </Next>
      </Navigation>
      <Listing
        pizzas={pizzas}
        setPizzas={setPizzas}
        cart={cart}
        setCart={setCart}
      />
    </Main>
  );
};

export default Home;
