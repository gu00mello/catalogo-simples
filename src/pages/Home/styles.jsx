import styled from "styled-components";

import { Colors } from "../../global/colors";

export const Main = styled.main`
  width: 100%;
  max-width: 1280px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  box-sizing: border-box;
  margin: 0 auto;
`;

export const Navigation = styled.nav`
  width: 90%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  box-sizing: border-box;
  margin: 10px auto;

  & {
    @media screen and (min-width: 1024px) {
      width: 600px;
    }
  }
`;

export const Page = styled.h1`
  font-size: 5vw;
  color: ${Colors.text_primary_color};
  font-weight: bold;
  margin: 0;

  & {
    @media screen and (min-width: 1024px) {
      font-size: 16px;
    }
  }
`;

export const Next = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  font-size: 3vw;
  color: ${Colors.text_primary_color};
  font-weight: bold;
  margin: 0;

  & {
    @media screen and (min-width: 1024px) {
      font-size: 12px;
    }
  }
`;

export const Arrow = styled.img`
  width: 12px;
  margin-left: 5px;

  & {
    @media screen and (min-width: 1024px) {
      width: 15px;
    }
  }
`;
