import { useEffect, useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Header from "../components/Header";
import Home from "../pages/Home";
import Cart from "../pages/Cart";

const Routers = () => {
  const [page, setPage] = useState(0);
  const [cart, setCart] = useState([]);

  useEffect(() => {
    let storageCart = JSON.parse(localStorage.getItem("cart"));

    if (storageCart) setCart(storageCart);
  }, []);

  return (
    <Router>
      <Header count={cart.length} />
      <Routes>
        <Route
          path="/"
          exact={true}
          element={
            <Home page={page} setPage={setPage} cart={cart} setCart={setCart} />
          }
        />
        <Route
          path="/cart"
          exact={true}
          element={<Cart cart={cart} setCart={setCart} />}
        />
      </Routes>
    </Router>
  );
};

export default Routers;
