// Export
export const Colors = {
  background_primary_color: "#f1f1f1",
  background_secundary_color: "#e0dbd7",
  background_primary_hover: "#ccc6c2",  
  background_clean_color: "#ffffff",

  button_primary_color: "#a83216",
  button_secundary_color: "#FF9D39",

  text_primary_color: "#1C1E21",
  text_secundary_color: "#ffffff",
  text_description_primary_color: "#c9c7c3",
  text_description_secundary_color: "#403e3a",
  text_name_primary_color: "#661c0a",
  text_promo_primary_color: "#f2af29",

  success_primary_color: "#2f932c",
  warning_primary_color: "#c91438",
};
